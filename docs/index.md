# Python 가상 환경: 설정과 사용 <sup>[1](#footnote_1)</sup>

<font size="3">Python 프로젝트와 종속성을 격리하기 위한 가상 환경을 만들고 사용하는 방법에 대해 알아본다.</font>

## 목차

1. [개요](./virtual-environments.md#intro)
1. [가상 환경이란 무엇이며 이를 사용하는 이유?](./virtual-environments.md#sec_02)
1. [`venv`으로 가상 환경 생성 방법](./virtual-environments.md#sec_03)
1. [가상 환경의 활성화와 비활성화 방법](./virtual-environments.md#sec_04)
1. [가상 환경에서 패키지를 설치와 관리 방법](./virtual-environments.md#sec_05)
1. [`requirements.txt`를 사용하여 종속성 지정 방법](./virtual-environments.md#sec_06)
1. [가상 환경과 패키지 관리를 위한 `pipenv` 사용 방법](./virtual-environments.md#sec_07)
1. [가상 환경의 생성과 관리를 위한 `conda` 사용 방법](./virtual-environments.md#sec_08)
1. [요약](./virtual-environments.md#summary)

<a name="footnote_1">1</a>: [Python Tutorial 35 — Python Virtual Environments: Setup and Usage](https://levelup.gitconnected.com/python-tutorial-35-python-virtual-environments-setup-and-usage-4181727e8684?sk=ae8fab3e04b89fc2f3c748997fd9c214)를 편역하였습니다.
