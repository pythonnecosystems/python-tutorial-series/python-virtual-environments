# Python 가상 환경: 설정과 사용

## <a name="intro"></a> 개요
Python 가상 환경에 대한 이 포스팅에서는 가상 환경이 무엇인지, 왜 유용한지, Python 프로젝트에서 사용하는 방법을 설명한다. 또한 가상 환경에 패키지를 설치하고 관리하는 방법과 가상 환경과 패키지를 관리하기 위해 `venv`, `pipenv`과 `conda` 같은 다양한 도구를 사용하는 방법도 다룬다.

이 포스팅을 학습하면 아래 작업을 수행할 수 있다.

- `venv`, `pipenv` 및 `conda`를 사용하여 가상 환경 생성과 활성화
- 가상 환경에 패키지 설치와 관리
- `requirements.txt`를 사용하여 프로젝트의 종속성 지정
- 가상 환경을 위한 다양한 툴의 장단점 파악

시작하기 전에 Python과 가상 환경에 관련된 기본 개념과 용어를 살펴 보겠다.

## <a name="sec_02"></a> 가상 환경이란 무엇이며 이를 사용하는 이유?
가상 환경은 자체적으로 설치된 패키지와 종속성 세트를 갖는 독립적이고 격리된 Python 환경이다. 가상 환경을 사용하면 글로벌 Python 설치나 다른 프로젝트에 영향을 주지 않고 서로 다른 Python 프로젝트에서 작업할 수 있다. 테스트, 개발, 생산 등 서로 다른 목적으로 여러 개의 가상 환경을 생성할 수 있다.

가상 환경을 사용하면 다음과 같은 많은 이점이 있다.

- 서로 다른 프로젝트 간의 패키지 충돌과 버전 불일치를 방지할 수 있다.
- 글로벌 Python 설치를 깨끗하고 최소화하고 각 프로젝트에 필요한 패키지만 설치할 수 있다.
- 사용할 패키지와 버전을 정확하게 지정하여 동일한 Python 환경을 다른 기계나 플랫폼에서 쉽게 재현할 수 있다.
- 기존 프로젝트를 깨뜨릴 위험 없이 새 패키지나 버전으로 실험할 수 있다.

이 포스팅에서는 `venv`, `pipenv`, `conda` 등 다양한 도구를 사용하여 가상 환경을 생성하고 사용하는 방법을 보일 것이다. 각 도구에는 각각의 장단점이 있으며 필요와 선호에 맞는 도구를 선택할 수 있다.

## <a name="sec_03"></a> `venv`으로 가상 환경 생성 방법
Python에서 가상 환경을 만드는 가장 간단하고 일반적인 방법 중 하나는 내장된 모듈 `venv`를 사용하는 것이다. `venv` 모듈을 사용하면 자체 사이트 디렉토리가 있고 글로벌 Python 설치에서 패키지를 선택적으로 상속받을 수 있는 경량 가상 환경을 만들고 관리할 수 있다.

`venv`를 사용하여 가상 환경을 생성하려면 다음 단계를 따라야 한다.

1. 프로젝트 디렉토리를 생성하고 해당 디렉토리로 이동합니다. 예를 들어 `my_project`라는 디렉토리를 생성하고 현재 작업 디렉토리를 해당 디렉토리로 변경한다.
1. `python -m venv env` 명령을 실행하여 프로젝트 디렉토리 내에 `env`라는 이름의 가상 환경을 생성한다. 가상 환경에 대해 원하는 이름을 사용할 수 있지만 `env`는 통상적인 규칙이다. 이 명령은 가상 환경을 구성하는 파일과 디렉토리를 포함하는 `env`라는 하위 디렉토리를 생성한다.
1. 운영 체제에 적합한 명령을 실행하여 가상 환경을 활성화한다. 예를 들어 Windows에서는 `env\Scripts\activate.bat`을 실행하고 리눅스 또는 macOS에서는 `source env/bin/activate`를 실행한다. 명령 프롬프트가 변경되어 가상 환경에 있음을 나타내는 것을 볼 수 있다. 예를 들어 Windows에서는 프롬프트로 `(env) C:\Users\User\my_project>`이 디스플레이된다.

일단 가상환경을 활성화하면 글로벌 Python 설치나 다른 가상환경에 영향을 주지 않고 그 안에 패키지를 설치하여 사용할 수 있다. 가상환경을 비활성화하기 위해서는 명령 프롬프트에서 단순히 `deactivate` 명령어를 실행하면 된다.

다음은 리눅스 또는 macOSs에서 `venv`를 사용하여 가상 환경을 만들고 활성화하는 방법의 예이다.

```bash
# Create a project directory
yjlee@YJLees-MacBook-Pro  ~  mkdir my_project
yjlee@YJLees-MacBook-Pro  ~  cd my_project
# Create a virtual environment named env
yjlee@YJLees-MacBook-Pro  ~/my_project  python -m venv env
# Activate the virtual environment
yjlee@YJLees-MacBook-Pro  ~/my_project  source env/bin/activate
(env) yjlee@YJLees-MacBook-Pro  ~/my_project 
```

## <a name="sec_04"></a> 가상 환경의 활성화와 비활성화 방법
가상 환경을 활성화한다는 것은 Python 인터프리터와 해당 가상 환경에 설치된 패키지 환경으로 전환하는 것을 의미한다. 가상 환경을 비활성화한다는 것은 설치된 글로벌 Python과 기본 패키지 환경으로로 다시 전환하는 것을 의미한다.

가상 환경을 활성화하려면 명령 프롬프트에서 운영 체제에 적합한 명령을 실행해야 한다. 예를 들어 Windows에서는 `env\Scripts\activate.bat`를 실행하고 리눅스 또는 macOS에서는 `source env/bin/activate`를 실행한다. 명령 프롬프트가 변경되어 가상 환경에 있음을 나타내는 것을 볼 수 있다. 예를 들어 macOS에서는 `(env) C:\Users\User\my_project>`이 디스플레이된다.

가상 환경을 비활성화하려면 명령 프롬프트에서 `deactivate` 명령을 실행하면 된다. 명령 프롬프트가 일반적인 것으로 다시 변경되는 것을 볼 수 있다. 예를 들어 macOS에서는 `yjlee@YJLees-MacBook-Pro  ~/my_project `와 같은 것이 디스플레이된다.

다음은 macOS에서 `venv`를 사용하여 가상 환경의 활성화와 비활성화 방법의 예이다.

```bash
# Activate the virtual environment
yjlee@YJLees-MacBook-Pro  ~/my_project  source env/bin/activate
(env) yjlee@YJLees-MacBook-Pro  ~/my_project 
# Deactivate the virtual environment
(env) yjlee@YJLees-MacBook-Pro  ~/my_project  deactivate
yjlee@YJLees-MacBook-Pro  ~/my_project 
```

패키지를 설치하거나 사용하기 전에 가상 환경을 활성화하는 것이 중요하다. 그렇지 않으면 설치된 글로벌 Python과 기본 패키지를 사용하게 된다. 명령 프롬프트에서 `python --version`과 `pip list` 명령을 실행하여 Python 버전과 어떤 패키지를 사용하고 있는지 확인할 수 있다.

## <a name="sec_05"></a> 가상 환경에서 패키지를 설치와 관리 방법
일단 가상 환경을 활성화하면, `pip` 명령을 사용하여 해당 환경에 패키지를 설치하고 관리할 수 있다. `pip`은 Python용 패키지 매니저로 PyPI(Python Package Index)나 다른 소스에서 패키지를 검색, 다운로드, 설치, 업데이트 및 제거할 수 있다.

`pip`을 사용하여 패키지를 설치하려면 `pip install package_name` 명령을 실행하면 된다. 여기서 `package_name`은 설치하려는 패키지의 이름이다. 예를 들어 인기 있는 데이터 분석 패키지 `panda`를 설치하려면 `pip install panda`를 실행하면 된다. `==` 연산자를 사용하여 설치하려는 패키지의 버전을 지정할 수도 있다. 예를 들어 panda version 1.3.4를 설치하려면 `pip install panda== 1.3.4`를 실행하면 된다.

`pip`을 사용하여 패키지를 업데이트하려면 `pip install — upgrade package_name` 명령을 실행한다. 여기서 `package_name`은 업데이트할 패키지의 이름이다. 예를 들어 pandas를 최신 버전으로 업데이트하려면 `pip install — upgrade panda`를 실행한다. 또한 `pip install — upgrade all`을 실행하여 가상 환경의 모든 패키지를 업데이트할 수도 있다.

`pip`을 사용하여 패키지를 제거하려면 `pip uninstall package_name` 명령을 실행하면 되며, 여기서 `package_name`은 제거할 패키지의 이름이다. 예를 들어 `panda`를 제거하려면 `pip uninstall panda`를 실행하면 된다. 제거를 진행하기 전에 제거를 확인하라는 메시지가 디스플레이된다.

가상 환경에 설치된 모든 패키지와 해당 버전을 나열하려면 명령어 `pip list`를 실행한다. 다음과 같은 내용이 디스플레이된다.

```bash
# List the packages and their versions in the virtual environment
yjlee@YJLees-MacBook-Pro  ~/my_project  pip list
Package    Version
---------- -------
numpy      1.21.2
pandas     1.3.4
pip        21.2.4
setuptools 57.4.0
```

PyPI에서 패키지를 검색하려면 `pip search package_name` 명령을 실행할 수 있으며, 여기서 `package_name`은 검색하려는 패키지 이름 또는 이름의 일부이다. 예를 들어 데이터 시각화와 관련된 패키지를 검색하려면 `pip search data visualization`을 실행한다. 다음과 같은 내용이 디스플레이된다.

```bash
# Search for packages related to data visualization on PyPI
yjlee@YJLees-MacBook-Pro  ~/my_project  pip search data visualization
plotly (5.3.1)                         - An open-source, interactive data visualization library for Python
seaborn (0.11.2)                       - Statistical data visualization
matplotlib (3.4.3)                     - Python plotting package
bokeh (2.3.3)                          - Interactive Data Visualization Library
altair (4.1.0)                         - Altair: A declarative statistical visualization library for Python.
...
```

## <a name="sec_06"></a> `requirements.txt`를 사용하여 종속성 지정 방법
`requirements.txt` 파일은 Python 프로젝트에 필요한 패키지와 그 버전을 나열한 텍스트 파일이다. 프로젝트의 종속성을 지정하고 문서화할 수 있고, `pip`을 사용하여 설치하기가 더 쉬워지는 편리한 방법이다.

`requirements.txt` 파일을 생성하려면 활성화된 가상 환경에 있는 동안 명령 프롬프트에서 `pip freeze > requirements.txt` 명령을 사용하여야 한다. 이렇게 하면 현재 작업 디렉토리에 `requirements.txt`라는 이름의 파일이 생성되며, 여기에는 가상 환경에 설치된 모든 패키지의 이름과 버전이 포함된다. 예를 들어 가상 환경에 `pandas`와 `numpy`를 설치한 경우 `requirements.txt` 파일은 다음과 같다.

```
# This is the content of the requirements.txt file
numpy==1.21.2
pandas==1.3.4
```

`requirements.txt` 파일에서 패키지를 설치하려면 활성화된 가상 환경에 있는 동안 명령 프롬프트에서 `pip install -r requirements.txt` 명령을 사용하면 된다. 그러면 `requirements.txt` 파일에 지정된 모든 패키지와 그 해당 버전이 설치된다. 예를 들어 프로젝트 디렉토리에 `requirements.txt` 파일이 있는 경우 다음 명령을 실행하여 종속성을 설치할 수 있다.

```bash
# Install the packages from the requirements.txt file
(env) yjlee@YJLees-MacBook-Pro  ~/my_project  pip install -r requirements.txt
```

`requirements.txt` 파일을 사용하면 Python 환경을 쉽게 공유, 재생 및 배포할 수 있으므로 프로젝트 종속성을 관리하는 데 유용한 방법이다. Git와 같은 버전 제어 도구를 사용하여 시간에 따른 `requirements.txt` 파일의 변화를 추적할 수도 있다.

## <a name="sec_07"></a> 가상 환경과 패키지 관리를 위한 `pipenv` 사용 방법
`pipenv`는 `pip`과 `venv`의 기능을 결합한 도구로, Python 프로젝트를 위한 가상 환경과 패키지를 모두 간단하고 우아한 방식으로 관리할 수 있다. `pipenv`는 프로젝트를 위한 가상 환경을 자동으로 생성하고 관리하며, 패키지를 설치하거나 제거할 때 `Pipfile`에서 패키지를 추가하거나 제거한다.

`pipenv`를 사용하려면 다음 단계를 따라야 한다.

1. `pip install pipenv` 명령을 사용하여 `pipenv`를 설치한다. 이 작업은 시스템에서 한 번만 수행하면 된다.
1. 프로젝트 디렉토리를 생성하고 해당 디렉토리로 이동한다. 예를 들어 `my_project`라는 디렉토리를 생성하고 현재 작업 디렉토리를 해당 디렉토리로 변경할 수 있다.
1. `pipenv install` 명령을 실행하여 프로젝트의 가상 환경과 `Pipfile`을 만든다. `Pipfile`은 프로젝트가 의존하는 패키지와 버전을 기록하는 파일이다. python 옵션을 사용하여 프로젝트에 사용할 Python 버전을 지정할 수도 있다. 예를 들어 Python 3.9를 사용하려면 `pipenv install —-python 3.9`를 실행하면 된다.
1. `pipenv shell` 명령을 실행하여 가상 환경을 활성화한다. 명령 프롬프트가 변경되어 가상 환경에 있음을 나타낸다. 예를 들어 `(my_project) yjlee@YJLees-MacBook-Pro  ~/my_project `와 같이 디스플레이된다.

일단 가상 환경을 활성화하면 `pipenv` 명령을 사용하여 패키지를 설치하고 관리할 수 있다. 예를 들어 인기 있는 웹 프레임워크 플라스크를 설치하려면 `pipenv install flask`를 실행하면 된다. 이렇게 하면 Flask와 그 종속성이 설치되고 `Pipfile`과 `Pipfile.lock`이 업데이트된다. `Pipfile.lock`은 모든 패키지의 정확한 버전과 해시, 그리고 그 종속성을 기록하는 파일로 프로젝트가 재현 가능하고 결정적임을 보장한다.

가상 환경을 비활성화하기 위해서는 명령 프롬프트에서 `exit` 명령을 실행하기만 하면 된다. 명령 프롬프트가 다시 일반적인 것으로 바뀌는 것을 볼 수 있을 것이다. 예를 들어 `yjlee@YJLees-MacBook-Pro  ~/my_project `와 같은 것을 볼 수 있을 것이다.

다음은 macOS에서 `pipenv`를 사용하는 예이다.

```
# Install pipenv
yjlee@YJLees-MacBook-Pro  ~  pip install pipenv

# Create a project directory
yjlee@YJLees-MacBook-Pro  ~  mkdir my_project
yjlee@YJLees-MacBook-Pro  ~  cd my_project

# Create a virtual environment and a Pipfile using Python 3.9
yjlee@YJLees-MacBook-Pro  ~/my_project  pipenv install --python 3.11
Virtualenv already exists!
Removing existing virtualenv...
Warning: the environment variable LANG is not set!
We recommend setting this in ~/.profile (or equivalent) for proper expected behavior.
Creating a virtualenv for this project...
Pipfile: /Users/yoonjoonlee/Pipfile
Using /Library/Frameworks/Python.framework/Versions/3.11/bin/python3 (3.11.4) to create virtualenv...
⠙ Creating virtual environment...created virtual environment CPython3.11.4.final.0-64 in 714ms
  creator CPython3Posix(dest=/Users/yoonjoonlee/.virtualenvs/yoonjoonlee-t6pw8uVK, clear=False, no_vcs_ignore=False, global=False)
  seeder FromAppData(download=False, pip=bundle, setuptools=bundle, wheel=bundle, via=copy, app_data_dir=/Users/yoonjoonlee/Library/Application Support/virtualenv)
    added seed packages: pip==23.2.1, setuptools==68.0.0, wheel==0.41.0
  activators BashActivator,CShellActivator,FishActivator,NushellActivator,PowerShellActivator,PythonActivator

✔ Successfully created virtual environment!
Virtualenv location: /Users/yoonjoonlee/.virtualenvs/yoonjoonlee-t6pw8uVK
Warning: Your Pipfile requires python_version 3.8, but you are using 3.11.4 (/Users/y/./y/bin/python).
  $ pipenv --rm and rebuilding the virtual environment may resolve the issue.
  $ pipenv check will surely fail.
Pipfile.lock not found, creating...
Locking [dev-packages] dependencies...
Locking [packages] dependencies...
Updated Pipfile.lock (db4242)!
Installing dependencies from Pipfile.lock (db4242)...
  🐍   ▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉ 0/0 — 00:00:00
To activate this project's virtualenv, run pipenv shell.
Alternatively, run a command inside the virtualenv with pipenv run.

# Activate the virtual environment
yjlee@YJLees-MacBook-Pro  ~/my_project  pipenv shell
Launching subshell in virtual environment...
 . /Users/yoonjoonlee/.virtualenvs/yoonjoonlee-t6pw8uVK/bin/activate
 yoonjoonlee@iMac4YJ  ~   . /Users/yoonjoonlee/.virtualenvs/yoonjoonlee-t6pw8uVK/bin/activate
(yjlee)  yjlee@YJLees-MacBook-Pro  ~ 

# Install Flask and update the Pipfile and the Pipfile.lock
(yjlee)  yjlee@YJLees-MacBook-Pro  ~  pipenv install flask
Warning: Your Pipfile requires python_version 3.8, but you are using 3.11.4 (/Users/y/./y/bin/python).
  $ pipenv --rm and rebuilding the virtual environment may resolve the issue.
  $ pipenv check will surely fail.
Installing flask...
Adding flask to Pipfile's [packages]...
✔ Installation Succeeded
Pipfile.lock (db4242) out of date, updating to (ac8e32)...
Locking [dev-packages] dependencies...
Locking [packages] dependencies...
Building requirements...
Resolving dependencies...
✔ Success!
Warning: /Users/yoonjoonlee/.local/pipx/venvs/pipenv/lib/python3.8/site-packages/pipenv/resolver.py:11: DeprecationWarning: pkg_resources is deprecated as an API. See https://setuptools.pypa.io/en/latest/pkg_resources.html
  import pkg_resources
/Users/yoonjoonlee/.local/pipx/venvs/pipenv/lib/python3.8/site-packages/pipenv/vendor/attr/_make.py:876: RuntimeWarning: Running interpreter doesn't sufficiently support code object introspection.  Some features like bare super() or accessing __class__ will not work with slotted classes.
  set_closure_cell(cell, cls)
Updated Pipfile.lock (ac8e32)!
Installing dependencies from Pipfile.lock (ac8e32)...
  🐍   ▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉ 0/0 — 00:0

# Deactivate the virtual environment
(yjlee)  yjlee@YJLees-MacBook-Pro  ~  exit
yjlee@YJLees-MacBook-Pro  ~/my_project 
```

## <a name="sec_08"></a> 가상 환경의 생성과 관리를 위한 `conda` 사용 방법
conda는 Python 프로젝트를 위한 가상 환경과 패키지를 모두 생성하고 관리할 수 있는 도구이다. conda는 PyPI에서 사용할 수 없는 복잡한 종속성과 이진 패키지를 처리할 수 있기 때문에 데이터 과학과 기계 학습 프로젝트에 특히 유용하다. conda는 R, C, C++ 등 다른 프로그래밍 언어와도 함께 작동한다.

conda를 사용하려면 다음 단계를 따라야 한다.

1. [공식 웹사이트](https://docs.conda.io/en/latest/)에서 운영 체제에 적합한 설치 프로그램을 다운로드하여 실행하여 conda를 설치한다. 이 작업은 시스템에서 한 번만 수행하면 된다.
1. 프로젝트 디렉토리를 생성하고 해당 디렉토리로 이동한다. 예를 들어 `my_project`라는 디렉토리를 생성하고 현재 작업 디렉토리를 해당 디렉토리로 변경할 수 있다.
1. `conda create — name env python=3.9` 명령을 실행하여 Python 3.9가 설치된 `env`라는 이름의 가상 환경을 만든다. 가상 환경에 원하는 이름을 사용할 수 있지만 `env`는 통상적인 규칙이다. 명령에 추가하여 가상 환경에 설치할 다른 패키지나 버전을 지정할 수도 있다. 예를 들어 `numpy`와 `pandas`를 설치하려면 `conda create — env python=3.9 numpy panda`를 실행한다.
1. `conda activate env` 명령을 실행하여 가상 환경을 활성화한다. 명령 프롬프트가 변경되어 가상 환경에 있음을 나타냅니다. 예를 들어 `(env) yjlee@YJLees-MacBook-Pro  ~/my_project `와 같이 나타난다.

가상 환경을 활성화하면 `conda` 명령을 사용하여 패키지를 설치하고 관리할 수 있다. 예를 들어 많이 알려진 웹 프레임워크 Flask를 설치하려면 `conda install flask`를 실행하면 된다. 그러면 Flask와 그 종속성이 설치되고 그에 따라 `environment.yml` 파일이 업데이트된다. `environment.yml` 파일은 프로젝트가 의존하는 패키지와 버전을 기록하는 파일이다. `pip` 명령을 사용하여 PyPI의 패키지를 `conda` 가상 환경에 설치할 수도 있지만 가능하면 언제든지 `conda`를 사용하는 것이 좋다.

가상환경을 비활성화하기 위해서는 명령 프롬프트에서 `conda deactivate` 명령어를 실행하기만 하면 된다. 명령 프롬프트가 다시 일반적인 것으로 바뀌는 것을 볼 수 있을 것이다. 예를 들어 `yjlee@YJLees-MacBook-Pro  ~/my_project `와 같은 것을 볼 수 있을 것이다.

다음은 Windows에서 conda를 사용하는 방법의 예이다.

```
# Install conda from the official website
C:\Users\user>download and run the installer
# Create a project directory
C:\Users\user>mkdir my_project
C:\Users\user>cd my_project
# Create a virtual environment named env with Python 3.9 and numpy
C:\Users\user\my_project>conda create --name env python=3.9 numpy
Collecting package metadata (current_repodata.json): done
Solving environment: done
## Package Plan ##
  environment location: C:\Users\user\anaconda3\envs\env
  added / updated specs:
    - numpy
    - python=3.9
```

```
The following packages will be downloaded:
    package                    |            build
    ---------------------------|-----------------
    ca-certificates-2021.10.8 |       haa95532_0         121 KB
    certifi-2021.10.8         |   py39haa95532_0         151 KB
    intel-openmp-2021.3.0     |    haa95532_3372         2.0 MB
    mkl-2021.3.0              |     haa95532_524       141.4 MB
    mkl-service-2.4.0         |   py39h2bbff1b_0          59 KB
    mkl_fft-1.3.1             |   py39h277e83a_0         175 KB
    mkl_random-1.2.2          |   py39hf11a4ad_0         289 KB
    numpy-1.21.2              |   py39hfca59bb_0          23 KB
    numpy-base-1.21.2         |   py39h0829f74_0         4.5 MB
    openssl-1.1.1l            |       h2bbff1b_0         4.8 MB
    pip-21.2.4                |   py37haa95532_0         1.8 MB
    python-3.9.7              |       h6244533_1        15.8 MB
    setuptools-58.0.4         |   py39haa95532_0         743 KB
    sqlite-3.36.0             |       h2bbff1b_0         1.1 MB
    tzdata-2021a              |       h5d7bf9c_0         121 KB
    vc-14.2                   |       h21ff451_1           6 KB
    vs2015_runtime-14.27.29016|       h5e58377_2         1.2 MB
    wheel-0.37.0              |     pyhd3eb1b0_1          31 KB
    wincertstore-0.2          |   py39haa95532_2          15 KB
    ------------------------------------------------------------
                                           Total:       174.4 MB
The following NEW packages will be INSTALLED:
  ca-certificates    pkgs/main/win-64::ca-certificates-2021.10.8-haa95532_0
  certifi            pkgs/main/win-64::certifi-2021.10.8-py39haa95532_0
  intel-openmp       pkgs/main/win-64::intel-openmp-2021.3.0-haa95532_3372
  mkl                pkgs/main/win-64::mkl-2021.3.0-haa95532_524
  mkl-service        pkgs/main/win-64::mkl-service-2.4.0-py39h2bbff1b_0
  mkl_fft            pkgs/main/win-64::mkl_fft-1.3.1-py39h277e83a_0
  mkl_random         pkgs/main/win-64::mkl_random-1.2.2-py39hf11a4ad_0
  numpy              pkgs/main/win-64::numpy-1.21.2-py39hfca59bb_0
  numpy-base         pkgs/main/win-64::numpy-base-1.21.2-py39h0829f74_0
  openssl            pkgs/main/win-64::openssl-1.1.1l-h2bbff1b_0
  pip                pkgs/main/win-64::pip-21.2.4-py37haa95532_0
  python             pkgs/main/win-64::python-3.9.7-h6244533_1
  setuptools         pkgs/main/win-64::setuptools-58.0.4-py39haa95532_0
  sqlite             pkgs/main/win-64::sqlite-3.36.0-h2bbff1b_0
  tzdata             pkgs/main/noarch::tzdata-2021a-h5d7bf9c_0
  vc                 pkgs/main/win-64::vc-14.2-h21ff451_1
  vs2015_runtime     pkgs/main/win-64::vs2015_runtime-14.27.29016-h5e58377_2
  wheel              pkgs/main/noarch::wheel-0.37.0-pyhd3eb1b0_1
  wincertstore       pkgs/main/win-64::wincertstore-0.2-py39haa95532_2
Proceed ([y]/n)? y
Downloading and Extracting Packages
numpy-base-1.21.2   | 4.5 MB    | ############################################################################ | 100%
pip-21.2.4          | 1.8 MB    | ############################################################################ | 100%
setuptools-58.0.4   | 743 KB    | ############################################################################ | 100%
mkl-service-2.4.0   | 59 KB     | ############################################################################ | 100%
mkl_fft-1.3.1       | 175 KB    | ############################################################################ | 100%
numpy-1.21.2        | 23 KB     | ############################################################################ | 100%
mkl_random-1.2.2    | 289 KB    | ############################################################################ | 100%
mkl-2021.3.0        | 141.4 MB  | ############################################################################ | 100%
python-3.9.7        | 15.8 MB   | ############################################################################ | 100%
sqlite-3.36.0       | 1.1 MB    |
```

## <a name="summary"></a> 요약
이 포스팅에서는 Python 프로젝트를 위해 가상 환경을 만들고 사용하는 방법을 설명하였다. 또한 가상 환경에 패키지를 설치하고 관리하는 방법, 가상 환경과 패키지를 관리하기 위해 `venv`, `pipenv`및 `conda` 도구를 사용하는 방법도 설명하였다.

가상 환경을 사용하면 Python 프로젝트와 종속성을 격리하고 패키지 충돌과 버전 불일치를 방지하며 Python 환경을 쉽게 재현하고 배포할 수 있다. 또한 기존 프로젝트를 어기는 위험을 감수하지 않고 새로운 패키지나 버전으로 실험할 수 있다.

<span style="color:blue">**Note**</span>: 통상적으로 `venv`를 사용하여 가상 환경을 사용하고 종속성을 관리한다. 따라서, 이에 대한 대한 설명을 정확하게 하였으나 다른 두 도구에 대하여는 설명에 오류가 있을 수 있다.
